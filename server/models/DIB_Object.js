var Model = require('./Model');

// ================================================================================
// === Create ===

exports.create = function(type, dibObject, callback) {   // callback(error)

	var model  = Model.get(type);
	var object = new model({ 'dibobject' : dibObject});
	object.save(function(error) {

		callback(error, object);
	});
}

// ================================================================================
// === Get All ===

exports.getAll = function(type, callback) {

	var model = Model.get(type);

	model.find( {}, function(error, objects) {

		callback(error, objects);
	});
}

// ================================================================================
// === Get One ===

exports.getOne = function(type, id, callback) {

	var model = Model.get(type);

	model.find( { '_id' : id}, function(error, objects) {

		callback(error, objects);
	});
}

// ================================================================================
// === Get Some ===

exports.getSome = function(type, condition, callback) {

	var model = Model.get(type);

	model.find(condition, function(error, objects) {

		callback(error, objects);
	});
}

// ================================================================================
// === Update ===

exports.update = function(type, id, dibObject, callback) {

	var model = Model.get(type);

	model.update({ '_id' : id }, { 'dibobject' : dibObject },  function(error, objects) {

		callback(error, objects);
	});
}

// ================================================================================
// === Delete ===

exports.remove = function(type, id, callback) {

	var model = Model.get(type);

	model.remove({ '_id' : id }, function(error) {
	 	if (!error) {
	            callback(null, true);
	    }
	    else {
			callback(error, false);
	    };
	});
}

// ================================================================================
// === Find one and update ===

exports.findOneAndUpdate = function(type, condition, dibObject, callback) {

	var model = Model.get(type);

	model.findOneAndUpdate(condition, { 'dibobject' : dibObject },  function(error, object) {

		callback(error, object);
	});
}

// ================================================================================
// === Find by Id ===

exports.findById = function(type, id, callback) {

	var model = Model.get(type);

	model.findById(id, function(error, object) {

		callback(error, object);
	});
}

// === End ===
// ================================================================================

