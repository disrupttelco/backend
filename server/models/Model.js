// ================================================================================
// === Requirements ===

var mongoose = require('mongoose');

var config = require('../../config/settings.cfg');

// ================================================================================
// === Database Connection ===

if (CONFIG.distMongoURL) {
	mongoose.connect(CONFIG.distMongoURL);
}

// ================================================================================
// === Test Connection ===

function testConnection(callback) {

	if (!models['_dummy_']) {

		var schema = new mongoose.Schema( { x : String } );

		models['_dummy_'] = mongoose.model('_dummy_', schema);
	}

	try {

		models['_dummy_'].find({ x : 'foo'}, function (error, ignore) {

			if (error) {

				console.log('Model.testConnection: ' + error);
				console.log('Model.testConnection: Attempt to disconnect ...');

				mongoose.disconnect(function() {

					console.log('testConnection: ... and re-connect ...');

					mongoose.connect(CONFIG.distMongoURL);

					callback(undefined, "Connection reestablished");
				});
			}
			else {

				callback(undefined, "Connection OK");
			}
		});
	}
	catch (error) {

		console.log('testConnection error: ' + error);
		callback(error, "Connection cannot be reestablished");
	}
}

// ================================================================================
// === Timer to Test Connection ===

if (CONFIG.distMongoURL) {
	setInterval(
		function() {
			try {

				testConnection(
					function (error, message) {

						if (error) {

							console.log(error);
						}
						else {

							// console.log(message);
						}
					});
			}
			catch (error) {

				console.log(error);
			}
		},
		10*60*1000);
}

// ================================================================================
// === Get Model ===

var models = {};

exports.get = function(type) {

	if (!models[type]) {

		// console.log("Create model for type: " + type);

		var schema = new mongoose.Schema( { dibobject  : {} } );

		models[type] = mongoose.model(type, schema);
	}

	return models[type];
}

// === End ===
// ================================================================================
