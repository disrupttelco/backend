var FCM = require('fcm-node');
var domain = require("domain");

// === Notify users with Push notifications ====================================

var token = "fri318cWY6E:APA91bFPkUCl2A9gKrPAi3bIP_lXK1lOgmGDR8qV3FpsarPKS8jpnTtG0K2R72RNTa35XwmE4D-EFy51KZhV5cGL2rROZA9u2nkQ6tOdcTfvwmkiXjaN9ydeJvJMPFFicKajT3L0z-9M";
exports.notify = function(req,res) {

	// Error handling with domains
	var d = domain.create();
	d.on('error', function(err) {
		console.error("notify: " + err);
		res.status(500).send(err);

	});
	d.run(function() {

		var serverKey = 'AIzaSyAS3t2DV8Wr5riL0QNDrMVtHtemlI0bnSc';
		var fcm = new FCM(serverKey);

		var title = req.body.title;
		var message = req.body.message;
		var picture = req.body.picture;

		var message = { 
		    to: token,  
	        priority : "high",  
		    notification: {
		        title: title, 
		        body: message,
		        sound: "default",
				"badge" : "1",
				"content_available" : true
		    },
		    data: {  //pictureLink
		        picture: picture
		    }
		};

		fcm.send(message, function(err, response){
		    if (err) {
		        throw ("Notification error: " + err);
		    } else {
		        res.send(200, "Successfully sent with response: " + response);
		    }
		});
	});

};

// === Register a new user in the database =====================================

exports.setToken = function(req,res) {

	// Error handling with domains
	var d = domain.create();
	d.on('error', function(err) {
		console.error("setToken: " + err);
		res.status(500).send(err);

	});
	d.run(function() {
		if (req.body.token) {
			token = req.body.token;
		} else{
			throw "no token!";
		}
		console.debug("setToken NEW TOKEN: " + token);
		res.status(200).send("ok, token set");
	});
};