// === Imports =================================================================

var log4js = require( 'log4js' );
var logger = log4js.getLogger();
var IndexController  = require('./../controllers/IndexController.js');
var UserController = require('./../controllers/UserController.js');

// =============================================================================
// === Routes ==================================================================
// =============================================================================

module.exports = function(app) {

	app.get('/', 										IndexController.render);
	app.post('/notify', 								UserController.notify);
	app.post('/setToken', 								UserController.setToken);

    logger.info("App routes have been included...");
};