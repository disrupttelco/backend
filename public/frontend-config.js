// === Angular module definition ===============================================

var app = angular.module('CONFIG', []);

app.constant("CONFIG", {
        builderLink: "https://apps.sahva.dk",
        architectLink: "https://arch.sahva.dk",
        loginLink: "https://login.sahva.dk",
        distLink: "https://dist.sahva.dk",
        mapLink: "https://dibinterface.sahva.dk",
        doclinks: {
        	dist: "https://distribution.advice2u.dk/dib/rest/viewartifact//advice2u/DIB-DOC//Distributor/bootstrap/index.html",
        	notifications:  "https://distribution.advice2u.dk/dib/rest/viewartifact//advice2u/DIB-DOC//Distributor/bootstrap/index.html?t=topics/DIBnotifications.html&tref=d1e15"
        }
});