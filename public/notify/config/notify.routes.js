angular.module('notify').config(['$routeProvider',
  function($routeProvider) {
    $routeProvider
    .when('/notify', {
      templateUrl: 'notify/views/send-notify.html'
    }).otherwise({
      redirectTo: '/notify'
    });
  }
]);