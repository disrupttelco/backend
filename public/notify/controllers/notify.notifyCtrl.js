angular.module('notify').controller('notifyCtrl', ['$scope','$http', '$routeParams', '$location','$window', '$rootScope', '$uibModal',
  function($scope, $http, $routeParams, $location, $window, $rootScope, $uibModal) {

  	// define an empty notification
  	$scope.notification = {
  		title: "",
  		message: "",
  		picture: "" 
  	};

  	//=== Send the notification ================================================

  	$scope.sendNotify = function() {

		$http.post('/notify', $scope.notification).success(function (data, status, headers, config) {

			$scope.results = data;

		}).error(function (data, status, headers, config) {

			$scope.results = data;
		});
  	};

}]);