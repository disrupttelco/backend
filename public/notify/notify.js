// === Angular module definition ===============================================

var app = angular.module('notify', ['angular-clipboard', 'ui.bootstrap', 'ngAnimate']);

// === Directive for using links to images =====================================

app.directive('suchHref', ['$location', function ($location) {
  return{
    restrict: 'A',
    link: function (scope, element, attr) {
      element.attr('style', 'cursor:pointer');
      element.on('click', function(){
        $location.path(attr.suchHref)
        scope.$apply();
      });
    }
  }
}]);

// === Filter to display Image sizes correctly =================================

app.filter('bytes', function() {
  return function(bytes, precision) {
    if (isNaN(parseFloat(bytes)) || !isFinite(bytes)) return '-';
    if (bytes=== 0) return '0 kB';
    if (typeof precision === 'undefined') precision = 1;
    var units = ['bytes', 'kB', 'MB', 'GB', 'TB', 'PB'],
      number = Math.floor(Math.log(bytes) / Math.log(1024));
    return (bytes / Math.pow(1024, Math.floor(number))).toFixed(precision) +  ' ' + units[number];
  }
});

// === Add token to each request automatically =================================

app.run(['$rootScope', '$injector', function($rootScope,$injector) {
    $injector.get("$http").defaults.transformRequest = function(data, headersGetter) {
        if ($rootScope.token2) {
          headersGetter()['xauthtoken'] = $rootScope.token2;
        }
        if (data) {
            return angular.toJson(data);
        }
    };
}]);


