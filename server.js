// === Dependencies ============================================================

var express = require('express');
var http     = require('http');
var https    = require('https');
var Path     = require('path');
var FS       = require('fs');
var cookieParser = require('cookie-parser');
var methodOverride = require('method-override');
var bodyParser = require('body-parser');
var morgan = require('morgan');
var session = require('express-session');

// =============================================================================
// === Parameters ==============================================================

var settingsFile = process.argv[2];

if (!settingsFile) {

    settingsFile = './config/settings.cfg';
}

if (!FS.existsSync(settingsFile)) {

    console.error("settings file' " + settingsFile + "' does not exist");
}

// =============================================================================
// === Configuration ===========================================================

CONFIG = require(settingsFile);

// =============================================================================
// === Port Number =============================================================

if (!CONFIG.PortNo) {

    CONFIG.PortNo = '3000';
}

// =============================================================================
// === Logging =================================================================

var log4js = require('log4js');

var log4jsConfFile = 'config/log4js-' + CONFIG.distPortNo + '.json';

if (FS.existsSync(log4jsConfFile)) {

    log4js.configure(log4jsConfFile);
}
else {

    var log4jsConfFile = 'config/log4js.json';

    if (FS.existsSync(log4jsConfFile)) {

		if (!FS.existsSync('logs')) {

			FS.mkdirSync('logs');
		}

        log4js.configure(log4jsConfFile);
    }
    else {

        console.log('No log4js configuration file found');
    }
}

var logger = log4js.getLogger();

// =============================================================================
// === Check Settings ==========================================================

    // === Test Mongo URL ===

    if (!CONFIG.MongoURL) {

        console.log("No defintion of parameter 'distMongoURL'");
    }


// =============================================================================
// === Application =============================================================

app = express();

// =============================================================================
// === Configuration ===========================================================

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cookieParser());
//app.use(express.favicon());
app.use(morgan('dev'));
app.use(methodOverride());
app.use(session({secret: 'SECRET',
    resave: true,
    saveUninitialized: true
}));
// enable CORS
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.set('port', process.env.PORT || CONFIG.PortNo);
app.set('views',Path.join(__dirname, '/server/views'));
app.set('view engine', 'ejs');
require('./server/routes/routes.js')(app); // routes
app.use(express.static('public'));

// =============================================================================
// === Listen ==================================================================

    var httpServer = http.createServer(app).listen(app.get('port'), function () {

        console.log('Express server listening on port ' + app.get('port'));
        //socketAction.addSocketRoute(httpServer);

    });